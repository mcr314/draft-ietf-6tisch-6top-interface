VERSION=03
DRAFT=draft-ietf-6tisch-6top-interface

${DRAFT}-${VERSION}.txt: ${DRAFT}.txt
	cp ${DRAFT}.txt ${DRAFT}-${VERSION}.txt
	git add ${DRAFT}-${VERSION}.txt ${DRAFT}.txt

${DRAFT}.xml: ${DRAFT}.no-yang.xml ietf-6top.yang
	(sed -n -e '1,/INSERT-YANG-BASE-MODEL-HERE/p' ${DRAFT}.no-yang.xml ; sed -e's/</\&lt;/' -e's/>/\&gt;/' ietf-6top.yang ; sed -n -e '/INSERT-YANG-BASE-MODEL-HERE/,$$p' ${DRAFT}.no-yang.xml ) | sed -e '/INSERT-YANG-BASE-MODEL-HERE/d'  >${DRAFT}.xml


%.txt: %.xml
	unset DISPLAY; XML_LIBRARY=$(XML_LIBRARY):./src xml2rfc $? $@

%.html: %.xml
	unset DISPLAY; XML_LIBRARY=$(XML_LIBRARY):./src xml2rfc --html -o $@ $?

