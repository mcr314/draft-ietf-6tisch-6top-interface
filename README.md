# README #

This is a document repository for the XML and supporting files for http://datatracker.ietf.org/doc/draft-ietf-6tisch-6top-interface/

## Abstract ##
   This document defines a generic data model for the 6TiSCH Operation
   Sublayer (6top), using the YANG data modeling language.  This data
   model can be used for future network management solutions defined by
   the 6TiSCH working group.  This document also defines a list of
   commands for the internal use of the 6top sublayer and/or to be used
   by implementers as an API guideline or basic specification.


### How to use this repository ###

On a Mac or Linux machine (or maybe windows with Cygnus? who knows. VMs are too cheap to meter),
just type "make" to format the xml into txt.  You'll need to have xml2rfc 2+ (I use 2.4.7+) installed.

The makefile assumes you have a git repo, and it will create a -0X.txt version as well as a .txt version.
The 0X-txt version will be added so that one has a history, the .txt version is more useful for running
diff on.  

To update the version number, tweak the VERSION= in the Makefile, and update the file names in the
xml file.

### Special considerations ###

The original xml code is kept in draft-ietf-6tisch-6top-interface.no-yang.xml. 
That file is processed by a simple cat/sed arrangement to insert the ietf-6top.yang file.